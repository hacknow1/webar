import {
  AmbientLight,
  AnimationMixer,
  Clock,
  Camera,
  Object3D,
  Scene,
  WebGLRenderer
} from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import ToolkitWorker from './vendor/artoolkit.worker';
import WebWorker from '../workerSetup';

export async function initAR(...args: any) {
  const [ container, marker, glbModel, video, inputWidth, inputHeight, canvasDraw, renderUpdate, trackUpdate ] = args;
  let mixers: any[] = [], world: any;

  const isMobile = () => /Android|mobile|iPad|iPhone/i.test(navigator.userAgent);

  const canvas_process = document.createElement("canvas");
  const context_process = canvas_process.getContext("2d");

  const vw = inputWidth;
  const vh = inputHeight;

  const pscale = 320 / Math.max(vw, (vh / 3) * 4);
  const sscale = isMobile() ? window.outerWidth / inputWidth : 1;

  const sw = vw * sscale;
  const sh = vh * sscale;

  video.style.width = sw + "px";
  video.style.height = sh + "px";

  container.style.width = sw + "px";
  container.style.height = sh + "px";

  canvasDraw.style.clientWidth = sw + "px";
  canvasDraw.style.clientHeight = sh + "px";
  canvasDraw.width = sw;
  canvasDraw.height = sh;

  const w = vw * pscale;
  const h = vh * pscale;
  const pw = Math.max(w, (h / 3) * 4);
  const ph = Math.max(h, (w / 4) * 3);
  const ox = (pw - w) / 2;
  const oy = (ph - h) / 2;

  canvas_process.style.width = pw + "px";
  canvas_process.style.height = ph + "px";
  canvas_process.width = pw;
  canvas_process.height = ph;

  const clock = new Clock();

  const interpolationFactor = 24;

  const trackedMatrix = {
    // for interpolation
    delta: [
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    ],
    interpolated: [
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    ]
  }

  const setMatrix = (matrix: any, value: any) => {
    let array: any[any] = [];

    for (const key in value) {
      array[key] = value[key];
    }
    if (typeof matrix.elements.set === "function") {
      matrix.elements.set(array);
    } else {
      matrix.elements = [].slice.call(array);
    }
  };

  // const context_draw = canvas_draw.getContext('2d');
  const renderer = new WebGLRenderer({
    canvas: canvasDraw,
    alpha: true,
    antialias: true
  });

  renderer.setPixelRatio(window.devicePixelRatio);

  const scene = new Scene();

  const camera = new Camera();
  camera.matrixAutoUpdate = false;
  // const camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 1, 1000);
  // camera.position.z = 400;

  scene.add(camera);

  const light = new AmbientLight(0xffffff);
  scene.add(light);

  const root = new Object3D();
  scene.add(root);

  /* Load Model */
  const threeGLTFLoader = new GLTFLoader();

  console.log('gonna load glb model');
  console.log(glbModel);

  threeGLTFLoader.load(glbModel, function (gltf) {
    const model = gltf.scene.children[0];
    model.position.z = 0;
    model.position.x = 100;
    model.position.y = 100;

    const animation = gltf.animations[0];
    const mixer = new AnimationMixer(model);
    mixers.push(mixer);
    const action = mixer.clipAction(animation);
    action.play();

    root.matrixAutoUpdate = false;
    root.add(model);
  });

  const worker = (new WebWorker(ToolkitWorker) as Worker);

  console.log(worker);

  const load = function() {
    renderer.setSize(sw, sh);

    console.log('load marker');
    console.log(worker);

    worker.postMessage({
      type: "load",
      pw: pw,
      ph: ph,
      marker: marker.url
    });

    worker.onmessage = function(event) {
      const msg = event.data;
      switch (msg.type) {
        case "loaded": {
          const proj = JSON.parse(msg.proj);
          const ratioW = pw / w;
          const ratioH = ph / h;
          proj[0] *= ratioW;
          proj[4] *= ratioW;
          proj[8] *= ratioW;
          proj[12] *= ratioW;
          proj[1] *= ratioH;
          proj[5] *= ratioH;
          proj[9] *= ratioH;
          proj[13] *= ratioH;
          setMatrix(camera.projectionMatrix, proj);

          console.log('marker loaded');
          break;
        }

        case "endLoading": {
          if (msg.end === true) {
            // removing loader page if present
            const loader = document.getElementById('loading');
            if (loader) {
              (loader.querySelector('.loading-text') as HTMLElement).innerText = 'Start the tracking!';

              setTimeout(function(){
                (loader.parentElement as HTMLElement).removeChild(loader);
              }, 2000);
            }
          }
          break;
        }

        case "found": {
          found(msg);
          break;
        }

        case "not found": {
          found(null);
          break;
        }
      }

      trackUpdate();
      process();
    }
  }

  const found = function(msg: any) {
    if (!msg) {
      world = null;
    } else {
      world = JSON.parse(msg.matrixGL_RH);
    }
  };

  let lasttime = Date.now();
  let time = 0;

  function process() {
    (context_process as CanvasRenderingContext2D).fillStyle = "black";
    (context_process as CanvasRenderingContext2D).fillRect(0, 0, pw, ph);
    (context_process as CanvasRenderingContext2D).drawImage(video, 0, 0, vw, vh, ox, oy, w, h);

    const imageData = (context_process as CanvasRenderingContext2D).getImageData(0, 0, pw, ph);
    worker.postMessage({ type: "process", imagedata: imageData }, [
      imageData.data.buffer
    ]);
  }

  const tick = function() {
    draw(time, lasttime);
    requestAnimationFrame(tick);

    if (mixers.length > 0) {
      for (let i = 0; i < mixers.length; i++) {
        mixers[i].update(clock.getDelta());
      }
    }
  };

  const draw = function(time: number, lasttime: number) {
    renderUpdate();
    const now = Date.now();
    const dt = now - lasttime;
    time += dt;
    lasttime = now;

    if (!world) {
      root.visible = false;
    } else {
      root.visible = true;

      // interpolate matrix
      for (let i = 0; i < 16; i++) {
        trackedMatrix.delta[i] = world[i] - trackedMatrix.interpolated[i];
        trackedMatrix.interpolated[i] =
          trackedMatrix.interpolated[i] +
          trackedMatrix.delta[i] / interpolationFactor;
      }

      // set matrix of 'root' by detected 'world' matrix
      setMatrix(root.matrix, trackedMatrix.interpolated);
    }

    renderer.render(scene, camera);
  };

  load();
  tick();
  process();

  return worker;
}
