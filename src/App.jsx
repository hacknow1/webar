import React from 'react';
import { Route } from 'react-router-dom';
import './App.css';

import { AR } from './components/AR';

function App() {
  return (
    <div className="App">
      <Route path='/:id'>
        <AR />
      </Route>
    </div>
  );
}

export default App;
