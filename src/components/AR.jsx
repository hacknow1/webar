import './AR.css';
import Model from '../Data/models/Flamingo.glb';

import React, { useEffect, useRef } from 'react';

import { initAR } from '../scripts/init';

export const AR = () => {
  const ARContainer = useRef(null);
  const ARVideo = useRef(null);
  const ARCanvas = useRef(null);

  const markers = {
    pinball: {
      width: 1637,
      height: 2048,
      dpi: 215,
      url: "../DataNFT/pinball"
    }
  };

  const glbModel = Model;

  useEffect(() => {
    const container = ARContainer.current;
    const video =  ARVideo.current;
    const canvas = ARCanvas.current;

    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      let hint = {
        audio: false,
        video: true
      };

      if (window.innerWidth < 800) {
        hint = {
          audio: false,
          video: {
            width: { ideal: 480 },
            height: { ideal: 640 }
            },
        };
      }

      navigator.mediaDevices.getUserMedia(hint).then(function(stream) {
        video.srcObject = stream;
        video.addEventListener("loadedmetadata", function() {
          video.play();

          initAR(
            container,
            markers.pinball,
            glbModel,
            video,
            video.videoWidth,
            video.videoHeight,
            canvas,
            function() {},
            function() {},
            null
          );
        });
      });
    }

    console.log(window);
  }, [glbModel, markers]);

  return (
    <div>
      hello AR
      <div id="ar_container" ref={ARContainer}>
        <video loop autoPlay muted playsInline id="ar_video" ref={ARVideo} />
        <canvas id="canvas" ref={ARCanvas} />
      </div>
    </div>
  )
}

